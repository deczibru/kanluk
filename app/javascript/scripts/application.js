$(document).ready(function($) {
    var menu = $(".menu-list"),
        offset = 10,
        upperPadding=10;
        menuItems = menu.find('a[href*="#"]'),
        scrollItems = menuItems.map(function(){
                    var href = $(this).attr("href"),
                        id = href.substring(href.indexOf('#')),
                        item = $(id);
            if (item.length) { return item;}
        });
    
    /* fancy scroll */
    
    menuItems.click(function(e){
        var href = $(this).attr("href"),
            id = href.substring(href.indexOf('#')),
            offsetTop = href === "#" ? 0 : $(id).offset().top;
            $('.site-content').stop().animate({
                scrollTop:offsetTop
            }, 300);
            e.preventDefault();
    });
    
    /*bind scroll action */
    $(".site-content").scroll(function(){
        var fromTop = $(this).scrollTop();
        
        /* get id of current scroll item */
        
        var cur = scrollItems.map(function(){
            if ($(this).offset().top <= fromTop-upperPadding)
                return this;
        });
        
        //get id of the current element
        
        cur = cur[cur.length-1];
        var id = cur && cur.length ? cur[0].id : "";
        
        menuItems.parent().removeClass("active");
        if(id){
            menuItems.parent().end().filter("[href*='#"+id+"']").parent().addClass("active");
        }
    })
    
})